package com.lean.simple_proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleProxyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleProxyApplication.class, args);
	}

}
