package com.lean.simple_proxy.controller;

import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://*")
@RestController
@RequestMapping("/api")
public class HeaderController {

    Logger LOG = LoggerFactory.getLogger(HeaderController.class);

    @GetMapping("/headers")
    public ResponseEntity<String> multiValue(
            @RequestHeader MultiValueMap<String, String> headers) {
        headers.forEach((key, value) -> {
            LOG.info(String.format(
                    "Header '%s' = %s", key, value.stream().collect(Collectors.joining("|"))));
        });
        return new ResponseEntity<String>("" +headers, HttpStatus.OK);
    }

    @PostMapping("/headers")
    public ResponseEntity<String> postmultiValue(
            @RequestHeader MultiValueMap<String, String> headers) {
        headers.forEach((key, value) -> {
            LOG.info(String.format(
                    "Header '%s' = %s", key, value.stream().collect(Collectors.joining("|"))));
        });
        return new ResponseEntity<String>("" +headers, HttpStatus.OK);
    }

    @PutMapping("/headers")
    public ResponseEntity<String> putmultiValue(
            @RequestHeader MultiValueMap<String, String> headers) {
        headers.forEach((key, value) -> {
            LOG.info(String.format(
                    "Header '%s' = %s", key, value.stream().collect(Collectors.joining("|"))));
        });
        return new ResponseEntity<String>("" +headers, HttpStatus.OK);
    }

    @DeleteMapping("/headers")
    public ResponseEntity<String> delmultiValue(
            @RequestHeader MultiValueMap<String, String> headers) {
        headers.forEach((key, value) -> {
            LOG.info(String.format(
                    "Header '%s' = %s", key, value.stream().collect(Collectors.joining("|"))));
        });
        return new ResponseEntity<String>("" +headers, HttpStatus.OK);
    }

}
