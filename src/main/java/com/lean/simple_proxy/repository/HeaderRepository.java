package com.lean.simple_proxy.repository;

import com.lean.simple_proxy.model.Header;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HeaderRepository extends JpaRepository<Header, Long> {
}
